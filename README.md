# Node Shop


### User Guide
To start the server, use the command line interface of your choice, navigate to the project directory, and run:
```console
# Install the required modules
npm install

# Start the server
npm start
```

To use the API,(to be continued).


### About
This is a RESTful API built with Node.js and MongoDB Atlas. I built this application by following along a YouTube series (https://www.youtube.com/playlist?list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q) in my efforts for teaching myself Node.js.
